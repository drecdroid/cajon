declare type FilterFunction = (this: any, value?: any, index?: number) => boolean;
declare type MapFunction<T, U> = (this: any, value?: T, index?: number) => U;
declare function noop(): IterableIterator<any>;
declare function map<T, U>(iterable: IterableIterator<T>, fn: MapFunction<T, U>, thisArg?: any): IterableIterator<any>;
declare function chain(...iterables: IterableIterator<any>[]): IterableIterator<any>;
declare function filter(iter: IterableIterator<any>, fn: FilterFunction, thisArg?: any): IterableIterator<any>;
declare function limit(iter: IterableIterator<any>, n: number): IterableIterator<any>;
declare class It<T> implements Iterable<T> {
    __iter__: IterableIterator<T>;
    constructor(iterable: IterableIterator<T>);
    map<U>(fn: MapFunction<T, U>): this;
    filter(fn: any): this;
    limit(n: any): this;
    [Symbol.iterator](): IterableIterator<T>;
    toArray(): T[];
    static from(iterable: any): It<{}>;
    static fromEnd(arrayLike: any): It<any>;
    static range(start: any, end: any, step?: number): It<number>;
    static repeat(toRepeat: any, n?: number): It<any>;
}
declare function range(start: number, end?: number, step?: number): IterableIterator<number>;
export { It, range, map, filter, noop, chain, limit };
