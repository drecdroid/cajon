"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function* noop() { }
exports.noop = noop;
function add(a, b) { return a + b; }
function* accumulate(iterable, fn = add) {
    let pv;
    for (let e of iterable) {
        if (pv === undefined) {
            yield e;
            pv = e;
        }
        else {
            pv = fn(pv, e);
            yield pv;
        }
    }
}
function* map(iterable, fn, thisArg = null) {
    let i = 0;
    for (let e of iterable) {
        yield fn.call(thisArg, e, i++);
    }
}
exports.map = map;
function* chain(...iterables) {
    for (let iter of iterables) {
        for (let e of iter) {
            yield e;
        }
    }
}
exports.chain = chain;
function* chainFrom(iterablesArray) {
    for (let iterable of iterablesArray) {
        for (let e of iterable) {
            yield e;
        }
    }
}
function* filter(iter, fn, thisArg = null) {
    let i = 0;
    for (let e of iter) {
        if (fn.call(thisArg, e, i++)) {
            yield e;
        }
    }
}
exports.filter = filter;
function* fromEnd(arrayLike) {
    if (arrayLike.length == null) {
        return 'Should have "length"';
    }
    let i = arrayLike.length - 1;
    while (i >= 0) {
        yield arrayLike[i--];
    }
}
function* limit(iter, n) {
    let i = 0;
    for (let e of iter) {
        if (i++ >= n) {
            break;
        }
        yield e;
    }
}
exports.limit = limit;
class It {
    constructor(iterable) {
        this.__iter__ = this.__iter__ = iterable || noop();
    }
    map(fn) {
        this.__iter__ = map(this.__iter__, fn);
        return this;
    }
    filter(fn) {
        this.__iter__ = filter(this.__iter__, fn);
        return this;
    }
    limit(n) {
        this.__iter__ = limit(this.__iter__, n);
        return this;
    }
    [Symbol.iterator]() {
        return this.__iter__;
    }
    toArray() {
        return [...this.__iter__];
    }
    static from(iterable) {
        return new It(iterable);
    }
    static fromEnd(arrayLike) {
        return new It(fromEnd(arrayLike));
    }
    static range(start, end, step = 1) {
        return new It(range(start, end, step));
    }
    static repeat(toRepeat, n = Infinity) {
        return new It(repeat(toRepeat, n));
    }
}
exports.It = It;
function* range(start, end, step = 1) {
    if (end == null) {
        let n = 0;
        while (n < start) {
            yield n;
            n += step;
        }
    }
    else if (start < end) {
        let n = start;
        while (n < end) {
            yield n;
            n += step;
        }
    }
    else {
        let n = start;
        while (n > end) {
            yield n;
            n -= step;
        }
    }
}
exports.range = range;
function* repeat(toRepeat, n = Infinity) {
    while (n-- > 0) {
        yield toRepeat;
    }
}
function* count(start, step = 1) {
    while (true) {
        yield start;
        start += step;
    }
}
function* cycle(iterable, n = Infinity) {
    while (n-- > 0) {
        for (let e of iterable) {
            yield e;
        }
    }
}
