type FilterFunction = (this : any, value ?: any, index?: number) => boolean
type MapFunction<T,U> = (this : any, value ?: T, index?: number) => U

function* noop() : IterableIterator<any> {}

function add(a : number, b:number ){ return a + b }

function* accumulate(iterable : IterableIterator<any>, fn=add){
    let pv
    for(let e of iterable){
        if(pv === undefined){
            yield e
            pv = e
        }
        else {
            pv = fn(pv, e)
            yield pv
        }
    }
}

function* map<T,U>(iterable : IterableIterator<T>, fn : MapFunction<T,U>, thisArg = null){

    let i=0
    for(let e of iterable){
        yield fn.call(thisArg, e, i++)
    }
}

function* chain(...iterables : IterableIterator<any>[]){
    for(let iter of iterables){
        for(let e of iter){
            yield e
        }
    }
}

function* chainFrom(iterablesArray : IterableIterator<any>[]){
    for(let iterable of iterablesArray){
        for(let e of iterable){
            yield e
        }
    }
}


function* filter(iter : IterableIterator<any>, fn : FilterFunction, thisArg = null){
    let i=0
    for(let e of iter){        
        if(fn.call(thisArg, e, i++)){
            yield e
        }
    }
}

function* fromEnd(arrayLike : ArrayLike<any>){
    if(arrayLike.length == null){ return 'Should have "length"'}
    let i=arrayLike.length-1
    while(i>=0){
        yield arrayLike[i--]
    }
}

function* limit(iter : IterableIterator<any>, n : number){
    let i=0
    for(let e of iter){
        if(i++ >= n){
            break;
        }
        yield e
    }
}

class It<T> implements Iterable<T>{
    __iter__ : IterableIterator<T>

    constructor(iterable : IterableIterator<T>){
        this.__iter__ = this.__iter__ = iterable || noop()
    }

    map<U>(fn : MapFunction<T, U>){
        this.__iter__ = map(this.__iter__, fn)
        return this
    }

    filter(fn){
        this.__iter__ = filter(this.__iter__, fn)
        return this
    }
    
    limit(n){
        this.__iter__ = limit(this.__iter__, n)
        return this
    }

    [Symbol.iterator](){
        return this.__iter__
    }

    toArray(){
        return [...this.__iter__]
    }

    static from(iterable){
        return new It(iterable)
    }

    static fromEnd(arrayLike){
        return new It(fromEnd(arrayLike))
    }

    static range(start, end, step=1){
        return new It(range(start, end, step))
    }

    static repeat(toRepeat, n=Infinity){
        return new It(repeat(toRepeat, n))
    }
}

function* range(start : number, end ?: number, step=1){
    if(end==null){
        let n=0
        while(n<start){
            yield n
            n+=step
        }
    }
    else if(start<end){
        let n=start
        while(n<end){
            yield n
            n+=step
        }
    }else{
        let n=start
        while(n>end){
            yield n
            n-=step
        }
    }
}

///// INFINITE ITERATORS

function* repeat<T=any>(toRepeat : T, n=Infinity){
    while(n-->0){
        yield toRepeat
    }
}

function* count(start : number, step=1){
    while(true){
        yield start
        start+=step
    }
}

function* cycle<T=any>(iterable : Iterable<T>, n=Infinity){
    while(n-->0){
        for(let e of iterable){
            yield e
        }
    }
}



export {
    It,    
    range,
    map,
    filter,
    noop,
    chain,
    limit
}