/**
 * USAGE
 * 
 * import * as obj from 'cajon/obj'
 */


function fromPairs(iterable, excludeNil=false){
    let obj={}
    for(let [k,v] of iterable){
        if(excludeNil && k == null){ continue; }
        obj[k]=v
    }
    return obj
}

function* toPairs(object){
    for(let k of object){
        yield [k, object[k]]
    }
}

export {
    fromPairs,
    toPairs
}
